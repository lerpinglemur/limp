import Color from './color';

/**
 * 
 * @param {Number} color - Breaks a color integer into an object with argb properties.
 * @returns { a:Number, r:Number, g:Number, b:Number }
 */
function toObject( color ) {

	return { a:(color>>24), r:(color>>16)&0xFF, g:(color>>8)&0xFF, b:(color & 0xFF) };

}

/**
 * 
 * @param {Number} color - Breaks a color integer into an array with argb color components
 * as its items.
 * @returns [ blue, green, red, alpha ]
 */
function toArray( color ) {

	return [ (color>>24), (color>>16)&0xFF, (color>>8)&0xFF, (color & 0xFF) ];

}

/**
 * 
 * @param {*} c1 
 * @param {*} c2 
 */
export function colorDiff( c1, c2 ) {

}