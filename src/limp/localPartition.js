import ColorSet from './colorSet';
import Color from './color';
import BSP from './colorBSP';
import Heap from 'heap.js';

/**
 * Parititon colors into classes based on nearby colors that
 * are within the same percentage tolerance.
 */
export default class LocalPartition {

	/**
	 * {Number} largest percent difference ( as numbers less than 1 )
	 * between two colors from the same ColorSet.
	 */
	get maxPercentDelta() { return this._maxDelta/255; }
	set maxPercentDelta(v) {

		if ( v >= 1 ) v = v/100;

		/// premultiply the percent by 255 to avoid divisions later.
		this._maxDelta = 255*v;

	}

	/**
	 * {Number} maximum color difference in a single color channel
	 * for two colors in the same color set.
	 */
	get channelDelta() { return this._chanDelta;}
	set channelDelta(v) { this._chanDelta = v; }

	/**
	 * maximum color value difference between two pixels of the same set.
	 */
	get maxDelta() { return this._maxDelta; }
	set maxDelta(v) { this._maxDelta = v; }

	/**
	 * {ImageData} data of the Image currently being partitioned.
	 */
	get imageData() { return this._imgData; }
	set imageData(v) { this._setImage(v); }

	/**
	 * {Uint8ClampedArray} the RGBA byte data for the current imageData.
	 */
	get colorData() { return this._colorData; }

	/**
	 * {UInt8ClampedArray} the RGBA byte data being written to.
	 */
	get _writableData() { return this._writeData;}

	get sets(){ return this._sets;}
	set sets(v) { this._sets = v;}

	get nodeSize() { return this._bsp.nodeSize; }
	set nodeSize(v) { this._bsp.nodeSize = v;}

	/**
	 * Returns the number of color sets in the partition.
	 */
	get count() { return this._sets.length; }

	constructor( opts=null ) {

		this._bsp = new BSP( this._nodeSize );
		this._sets = [];

		if ( opts ) Object.assign( this, opts );
		else {
			this._nodeSize = 16;
			this._maxDelta = 20;
		}

	}

	/**
	 * Sorts the sets array in order of the number of pixels assigned
	 * to each set.
	 */
	sort() {

		this._sets.sort( (a,b)=>{
	
			if ( a.pixelCount > b.pixelCount ) return -1;	// higher count comes first.
			else if ( a.pixelCount < b.pixelCount ) return 1;
			return 0;

		});

	}

	/**
	 * Sets the image whose pixels are being partitioned.
	 * @param {ImageData} image 
	 */
	_setImage( image ) {

		this._imgData = image;
		this._width = image.width;
		this._height = image.height;
		this._colorData = image.data;

	}

	/**
	 * Partition which combines similar colors, regardless of how
	 * far apart they are in the original image.
	 * TOO SLOW AND NOT USEFUL.
	 * @param {ImageData} image 
	 */
	globalPartition() {

		this._bsp.clear();

		let max = this._colorData.length;
		for( let ind = 0; ind < max; ind += 4 ) {

			var color = this.byteToColor(ind);

			var nearest = this._bsp.nearest( color, this._maxDelta );
			if ( nearest !== null ) {

				nearest.addColor( color );

			} else {
				this._bsp.add( new ColorSet(color));
			}

		} // for.

		this._sets = this._bsp.toArray();

	}

	/**
	 * Form a partition by creating locally connected colorSets, then merging
	 * them with any ColorSets in a bsp.
	 * @param {ImageData} image 
	 */
	localMerge() {

		this._bsp.clear();

		this._visited = [];
		this._fringe = [];
		
		let w = this._width;
		let max = w*this._height;
		for( let ind = 0; ind < max; ind++ ) {

			if ( this._visited[ind] ) continue;

			var colSet = this.setSearch({

					x: ind % w,
					y:Math.floor( ind/w)

			});

			var nearest = this._bsp.nearest( colSet._baseColor, this._maxDelta );
			if ( nearest !== null ) {

				// merge new colors into existing set.
				nearest.merge( colSet );

			} else this._bsp.add( colSet );
			
		}

		this._sets = this._bsp.toArray();

	}

	/**
	 * Partition the colors found in an image.
	 * @param {*} image 
	 */
	partition() {

		var newSets = [];

		this._visited = [];
		this._fringe = [];

		let w = this._width;
		let max = w*this._height;
		for( let ind = 0; ind < max; ind++ ) {

			if ( this._visited[ind] ) continue;

			newSets.push( this.setSearch({

					x: ind % w,
					y:Math.floor( ind/w)

			}) );

		}

		this._sets = newSets;

	} //

	/**
	 * Returns new ImageData for the current Image, by using the color
	 * from the closest colorSet.
	*/
	globalRecolor() {

		if ( this.imageData === null || this.count < 1 ) return null;

		// width*height*rgba
		let max = 4*this.imageData.width*this.imageData.height;
		// prefill alpha.
		let newData = new Uint8ClampedArray( max ).fill(255);

		for( let ind = 0; ind < max; ind += 4 ) {

			var colSet = this._bsp.nearest( this.byteToColor(ind), 777 );
			this.putColor( newData, ind, colSet.color );
			
		}

		return new ImageData( newData, this.imageData.width, this.imageData.height );

	}

	/**
	 * Merge existing colorSets until there are maxSets or
	 * fewer in the BSP.
	 * @param {Number} maxSets 
	 */
	mergeSets( maxSets ) {

		let len = this._sets.length;

		let heap = new Heap();

		// marked which sets were already merged out.
		let used = new Map();

		var curSet, pair;

		// prefill heap.
		for( let i = 0; i < len; i++ ) {

			curSet = this._sets[i];
			pair = this._bsp.nearestSet( curSet );

			heap.add( pair, pair.dist );

		}

		let merges = this._sets.length - maxSets;
		while ( merges > 0 ) {

			pair = heap.getMin();
			if ( pair === undefined ) {
				console.log('ERROR:: MERGE HEAP EMPTY');
				break;
			}

			// Note that if pair.src was already the target of a merge, its
			// new parent will have been reentered in the queue.
			if ( used.has(pair.dest) === false && used.has(pair.src) === false ) {

				//if (used.has(pair.src))

				this._bsp.remove( pair.dest );

				curSet = this._topSet( used, pair.src);
	
				curSet.merge( pair.dest );
				used.set( pair.dest, curSet );
				merges--;


				pair = this._bsp.nearestSet( curSet );
				if ( pair.dest ) {

					if ( used.has( pair.dest ) ) console.log('ERROR: RE-ADDING DEST');
					else heap.add( pair, pair.dist );

				} else console.log('ERR: dest is null');

			} else {

				// set was already merged.

			}

			// NOTE: problem with A->B<-C with dist(A,B) and dist(B,C) at heap top.
			// This could cause A and C to merge too early.

		}

		// take only the top sets.
		this.sets = this._bsp.toArray();

	}

	/**
	 * Get the set at the top of a merge chain.
	 * @param {Map} map 
	 * @param {*} curSet 
	 */
	_topSet( map, curSet ) {

		let parent = map.get(curSet);
		while ( parent !== undefined ){

			curSet = parent;
			parent = map.get(parent);

		}
		return curSet;
	}

	/**
	 * For each ColorSet, calls a function whose result will be used
	 * as the representative color for that set. If no function is
	 * specified, colorSet.mean() will be used.
	 * @param {} func 
	 */
	calcSetColors( func=null ) {

		if ( !func ) func = ColorSet.prototype.mean;

		let max = this._sets.length;
		for( let i = 0; i < max; i++ ) {

			this._sets[i].color = func.apply( this._sets[i] );
			//console.log('set color: ' + this._sets[i].color );

		}

	}

	/**
	 * Begin a new color set at x,y, including all pixels
	 * that are less than a given color difference from
	 * the starting point.
	 * @param {Number} x - starting pixel x.
	 * @param {Number} y - starting pixel y.
	 */
	setSearch( pt ) {

		let curSet = new ColorSet( this.getPixel(pt.x,pt.y), pt );

		this.markUsed( pt.x,pt.y );

		this._fringe.push(pt);

		while ( this._fringe.length > 0 ) {

			pt = this._fringe.pop();

			( pt.x > 0 ) ? this.tryAdd( curSet, pt.x-1, pt.y ) : null;
			( pt.x < this._width-1 ) ? this.tryAdd( curSet, pt.x+1, pt.y ) :null;

			( pt.y > 0 ) ?this.tryAdd( curSet, pt.x, pt.y-1 ) : null;
			( pt.y < this._height-1 )? this.tryAdd( curSet, pt.x, pt.y+1 ) : null;

		}

		return curSet;

	}

	/**
	 * Attempt to add the pixel at the given x,y coordinate to the given set.
	 * @param {ColorSet} set 
	 * @param {number} x 
	 * @param {number} y 
	 */
	tryAdd( curSet, x, y ) {

		if ( this.wasUsed(x,y ) ) return false;

		var col = this.getPixel(x,y);

		//if ( (y==x) && ( y%20 == 0) ) console.log(`${x},${y}: ${col}`);
		//if ( (y==x) && ( y%20 == 0) ) console.log(curSet.color.toString() );

		if ( curSet.contains(col) || this._testColor(curSet.baseColor, col ) ) {

			curSet.addIntColor( col );

			this._fringe.push( {x:x, y:y });
			this.markUsed( x,y );

		}

	}

	_testColor( color, n ) {
		return color.absDiffInt(n) <= this._maxDelta;
	}

	/**
	 * Place the given color data in the writable imageData array.
	 * @param {Uint8ClampedArray} data 
	 * @param {Number} index 
	 * @param {Color} color 
	 */
	putColor( data, index, color ) {

		data[index++] = color.r;
		data[index++] = color.g;
		data[index] = color.b;

	}

	/**
	 * Returns the starting byte index of colorData for the given pixel
	 * index.
	 * @param {Number} x 
	 * @param {Number} y 
	 */
	byteIndex( x, y ) {
		// bitshift 2 == multiply by 4.
		return ( y*this._width + x ) << 2;
	}

	/**
	 * Returns the Color object stored at the given bye index
	 * of the colorData byte-array.
	 * @param {Number} byteInd 
	 */
	byteToColor( byteInd ) {
		return new Color( this._colorData[byteInd], this._colorData[byteInd+1], this._colorData[byteInd+2]);
	}

	/**
	 * Returns the RGB color which starts at the given byte index
	 * in colorData byte-array.
	 * @param {Number} bIndex 
	 */
	byteToPixel( bIndex ) {
		return (this._colorData[bIndex] << 16) + ( this._colorData[bIndex+1] << 8) + this._colorData[bIndex+2];
	}

	/**
	 * Returns the color pixel at the given pixel-index, where
	 * the index runs from 1 to ( width x height ).
	 * @param {Number} ind 
	 */
	indexToPixel( ind ) {
		ind = ind << 2;		// 4 bytes of of colorData for every pixel.
		return (this._colorData[ind] << 16) + ( this._colorData[ind+1] << 8) + this._colorData[ind+2];
	}

	/**
	 * Get the RBG pixel color at the x,y coordinate of the image.
	 */
	getPixel( x, y ) {

		let ind = ( y*this._width + x ) << 2;
		return (this._colorData[ind] << 16) + ( this._colorData[ind+1] << 8) + this._colorData[ind+2];
	}

	/**
	 * Gets the ARBG pixel color at the x,y coordinate of the image.
	*/
	getPixel32( x, y ) {

		let ind = ( y*this._width + x ) << 2;
		return (this._colorData[ind] << 16) + ( this._colorData[ind+1] << 8) + this._colorData[ind+2] + ( this._colorData[ind+3] << 24);
	}

	/**
	 * tests if a point was already assigned to a colorSet.
	 * @param {number} x
	 * @param {number} y
	 */
	wasUsed( x, y ) {
		return this._visited[ y*this._width + x ];
	}

	markUsed(x,y) { this._visited[ y*this._width +x ] = 1; }

	/**
	 * Returns the array index to mark that an x,y pixel has been visited
	 * by the algorithm.
	 * @param {number} x 
	 * @param {number} y 
	 */
	_index( x, y ) { return y*this._width + x; }
}