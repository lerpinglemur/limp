export default class ColorGroup {

	/**
	 * sets a function that tests whether colors should be included
	 * in the color group.
	 * { function( number, [img] ):boolean } testing method.
	 */
	get includeTest( test ){
		this._test = test;
	}

	/**
	 * Method used to resolve the color group.
	 */
	get resolveFunc() {
	}
	set resolveFunc(v) { this._resolve = v;}


	/**
	 * {number} Resolved color which all colors in the group should map to.
	 */
	get outColor() { return this._color; }

	/**
	 * Get the array of pixels included in this color group.
	 */
	get pixels() {}

	constructor() {
	}

	/**
	 * resolve all colors in the group to a single color output.
	 */
	resolve() {
	}

};