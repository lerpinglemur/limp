import Color from './color';
import ColorSet from './colorSet';

class ColorNode {

	/**
	 * Center point of the node.
	 */
	get center() { return this._center; }

	/**
	 * Vector which splits the node color-space.
	 * The split is taken along the line that goes from the center point
	 * of the parent node, to the center point of this node.
	 */
	get vector() { return this._vector }

	/**
	 * Space with positive cross relative to split vector.
	 */
	get pos() { return this._pos; }

	//get _farPlane() { return this._farSide;}
	//set _farPlane(v) { this._farSide = v;}

	/**
	 * Space with negative cross-product relative to split vector.
	 */
	get neg() { return this._neg; }

	get sets() { return this._sets; }

	/**
	 * {Number} number of its in this node or its subnodes.
	 */
	get count() { return this._sets ? this._sets.count : this._pos.count + this._neg.count; }

	get depth() { return this._depth; }

	get isLeaf() { return this._sets !== null; }

	constructor(parentCenter, items = [], depth = 0) {

		this._pCenter = parentCenter;
		this._sets = items;
		this._depth = depth;

	}

	add(colSet) {
		this._sets.push(colSet);
	}

	/**
	 * 
	 * @param {Color} target - Target Color to find (or best approximation.)
	 * @param {Object} best - { colorSet:ColorSet, dist:Number } - Current closest set to target.
	 * @param {Boolean} farSide - Whether this Node is already on the far side
	 * of a half-plane from the target point.
	 */
	nearest(target, best, farSide) {

		if (this.isLeaf) {

			for (let cSet of this._sets) {

				var dist = target.dist(cSet.color);
				if (dist < best.dist) {
					best.colorSet = cSet;
					best.dist = dist;
				}
			} // for-loop.

		} else {

			var cross = target.subCross(this._center, this._vector);

			/**
			 * Search the split-side containing the color, and conditionally
			 * search the opposite split-side if it might have items in range.
			 */
			if (cross >= 0) {

				this.pos.nearest( target, best, false );

				// distance to split-line is less than the search-inclusion distance
				// so the opposite side must also be checked.
				if (cross < best.dist) {

					if ( farSide === true ) {
						// when a node is split twice on the far-side from the target color,
						// and has a vector in the opposite direction of the target,
						// can check the distance to the parent split point.
						var dot = target.subDot( this._pCenter, this._vector );
						if ( dot < 0 && dot*dot > best.dist*best.dist ) return;

					}
					this.neg.nearest( target, best, true );

				}

			} else {

				this.neg.nearest( target, best, false );

				if (-cross > best.dist) {

					if ( farSide === true ) {
						var dot = target.subDot( this._pCenter, this._vector );
						if ( dot < 0 && dot*dot > best.dist*best.dist ) return;
					}
					this.pos.nearest( target, best, true );
				}
			}

		} // non-leaf.

	}

	remove(colSet) {

		let ind = this._sets.indexOf(colSet);
		// quick splice
		if ( ind < this._sets.length-1 ) this._sets[ind] = this._sets.pop();
		else this._sets.pop();

		return this._sets.length;

	}

	/**
	 * Merge child nodes back into this node.
	 */
	merge() {

		this._sets = this._pos._sets.concat(this.neg._sets);

		this._pos = this._neg = null;
		this._center = this._vector = null;

	}

	split() {

		this._center = this.calcCenter();
		this._vector = this._center.subtract(this._pCenter).normalizeRGB();

		let pos = [], neg = [];

		for (let curSet of this._sets) {

			if (curSet.color.subCross(this._center, this._vector) >= 0) pos.push(curSet);
			else neg.push(curSet);

		}

		this._pos = new ColorNode(this._center, pos, this._depth + 1);
		this._neg = new ColorNode(this._center, neg, this._depth + 1);

		//console.log('Split Depth: ' + this._depth );

		this._sets = null;

	}

	cross(colorSet) {
		return colorSet.color.subCross(this._center, this._vector);
	}

	calcCenter() {

		// A split should not even be occuring if the set length is 0.
		if (this._sets.length === 0) return new Color();

		let curColor;
		let rTot = 0, gTot = 0, bTot = 0;

		for (let curSet of this._sets) {

			curColor = curSet.color;
			rTot += curColor.r;
			gTot += curColor.g;
			bTot += curColor.b;

		}

		return new Color(rTot / this._sets.length, gTot / this._sets.length, bTot / this._sets.length);

	}

}

/**
 * Binary Space Partition of ColorSets, treating colors geometrically.
 */
export default class ColorBSP {

	get root() { return this._root; }

	get nodeSize() { return this.NODE_SIZE; }
	set nodeSize(v) {
		this.NODE_SIZE = v;
		this.SPLIT_SIZE = v / 2;
	}

	constructor(max_node_size = 8) {

		this.nodeSize = max_node_size;
		this._root = new ColorNode(new Color());

	}

	/**
	 * Empty the BSP.
	 */
	clear() {
		this._root = new ColorNode(new Color());
	}

	/**
	 * Returns a flat array of all elements in the BSP.
	 */
	toArray() {

		let result = [];
		let stack = [this._root];

		while (stack.length > 0) {

			var node = stack.pop();
			if (node.isLeaf) {

				result.push.apply(result, node.sets);

			} else stack.push(node.pos, node.neg);

		}

		return result;

	}

	/**
	 * UNUSED. Node-recursive version / worse performance.
	 * @param {*} targetSet 
	 * @param {*} maxDist 
	 */
	/*nearest( targetColor, maxDist ) {
		let best = { colorSet:null, dist:maxDist };
		this._root.nearest( targetColor, best, false );

		return best.colorSet;

	}*/

	/**
	 * Return the ColorSet nearest to this ColorSet.
	 * @param {ColorSet} targetSet - set to come closest to ( without matching exactly. )
	 */
	nearestSet( targetSet ) {

		let targetColor = targetSet.color;
		let stack = [ this._root ];
		let test, minDist = Number.MAX_VALUE;

		let best = null;

		while (stack.length > 0) {

			var node = stack.pop();

			if (node.isLeaf) {

				for (let cSet of node.sets) {

					test = targetColor.dist(cSet.color);
					if ( test < minDist && cSet != targetSet ) {
						best = cSet;
						minDist = test;
					}
				} // for-loop.

			} else {

				test = targetColor.subCross(node._center, node._vector);
				if (test >= 0) {

					node.pos._farPlane = false;
					stack.push( node.pos );

					// dist to split-line is less than the dist to target
					// so the opposite side must also be checked.

					if (test < minDist) {
						
						if ( node._farPlane === true ) {
							// a node opposite two consecutive splits from target, and facing away from target,
							// can test distance to the nearest point.
							test = targetColor.subDot( node._pCenter, node._vector );
							if ( test < 0 && test*test > minDist*minDist ) continue;
						}
						node.neg._farPlane = true;
						stack.push( node.neg);
					}

				} else {

					node.neg._farPlane = false;
					stack.push( node.neg );
					if (-test > minDist){

						if ( node._farPlane === true ) {
							test = targetColor.subDot( node._pCenter, node._vector );
							if ( test < 0 && test*test > minDist*minDist ) continue;
						}
						node.pos._farPlane = true;
						stack.push( node.pos);

					}

				}

			}

		} // while-loop.

		return { src:targetSet, dest:best, dist:minDist };

	}

	/**
	 * Return the ColorSet nearest to this color. The ColorSet
	 * must at least be within the target distance.
	 * @param {Color} targetColor
	 * @param {Number} maxDist 
	 */
	nearest(targetColor, maxDist) {

		let stack = [ this._root ];
		let best = null;
		var test;

		while (stack.length > 0) {

			var node = stack.pop();

			if (node.isLeaf) {

				for (let cSet of node.sets) {

					test = targetColor.dist(cSet.color);
					if (test < maxDist) {
						best = cSet;
						maxDist = test;
					}
				} // for-loop.

			} else {

				test = targetColor.subCross(node._center, node._vector);
				if (test >= 0) {

					node.pos._farPlane = false;
					stack.push( node.pos );

					// dist to split-line is less than the dist to target
					// so the opposite side must also be checked.

					if (test < maxDist) {
						
						if ( node._farPlane === true ) {
							// a node opposite two consecutive splits from target, and facing away from target,
							// can test distance to the nearest point.
							test = targetColor.subDot( node._pCenter, node._vector );
							if ( test < 0 && test*test > maxDist*maxDist ) continue;
						}
						node.neg._farPlane = true;
						stack.push( node.neg);
					}

				} else {

					node.neg._farPlane = false;
					stack.push( node.neg );
					if (-test > maxDist){

						if ( node._farPlane === true ) {
							test = targetColor.subDot( node._pCenter, node._vector );
							if ( test < 0 && test*test > maxDist*maxDist ) continue;
						}
						node.pos._farPlane = true;
						stack.push( node.pos);

					}

				}

			}

		} // while-loop.

		return best;

	}

	/**
	 * Return all color sets within a given distance of the given color.
	 * @param {Color} targetColor
	 * @param {Number} dist 
	 */
	near(targetColor, dist) {

		let stack = [this._root];
		let results = [];

		while (stack.length > 0) {

			var node = stack.pop();
			if (node.isLeaf) {

				for (let cSet of node.sets) {

					if (cSet.color.dist(targetColor) < dist) results.push(cSet);

				} // for-loop.

			} else {

				// distance from color to center color of node.
				var cross = targetColor.subCross(node.center, node.vector);

				/**
				 * Search the split-side containing the color, and conditionally
				 * search the opposite split-side if it might have items in range.
				 */
				if (cross >= 0) {

					// distance to split-line is less than the search-inclusion distance
					// so the opposite side must also be checked.
					if (cross < dist) stack.push(node.pos, node.neg);
					else stack.push(node.pos);

				} else {

					if (-cross < dist) stack.push(node.neg, node.pos);
					else stack.push(node.neg);

				}

			}

		} // while-loop.

		return results;

	}

	add(colSet) {

		let node = this._root;

		while (!node.isLeaf) {

			if (node.cross(colSet) >= 0) node = node.pos;
			else node = node.neg;

		}

		node.add(colSet);
		if (node.sets.length > this.NODE_SIZE) node.split();

	}

	/**
	 * Removes the first item found and returns it.
	 * Useful when iterating the tree while removing
	 * the iterated elements.
	 */
	removeOne() {

		let parent = null;
		let child = this._root;

		while (!child.isLeaf) {

			parent = child;

			if (parent.cross(colSet) >= 0) child = parent.pos;
			else child = parent.neg;

		}

		let it = child.sets.pop();
		if (parent !== null && parent.count < this.SPLIT_SIZE) {
			parent.merge();
		}
		return it;

	}

	remove(colSet) {

		let parent = null;
		let child = this._root;

		while (!child.isLeaf) {

			parent = child;

			if (parent.cross(colSet) >= 0) child = parent.pos;
			else child = parent.neg;

		}

		child.remove(colSet);
		if (parent !== null && parent.count < this.SPLIT_SIZE) {
			parent.merge();
		}

	}

};