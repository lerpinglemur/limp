export default class Color {

	/**
	 * Parses an RGB integer into a Color object.
	 * @param {*} n 
	 */
	static ParseRGB( n ) {
		return new Color( (n>>16)&0xFF, (n>>8)&0xFF, (n & 0xFF) );
	}

	toJSON() {
		return { r:this.r, g:this.g, b:this.b };
	}

	constructor( r=0, g=0, b=0 ) {

		this.r = r;
		this.g = g;
		this.b = b;

	}

	clone() { return new Color( this.r, this.g, this.b ); }

	setTo( r, g, b ) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	setRGB( n ) {
		this.r = (n>>16)&0xFF;
		this.g = (n>>8)&0xFF;
		this.b = (n & 0xFF);
	}

	toCSS() {
		return '#' + this.toRGB().toString(16).padStart(6,'0');
	}

	toString() {
		return ( this.toRGB() >>> 0).toString(16);
	}

	/**
	 * Converts the color object to an rgb integer. Alpha is ignored.
	 * @returns {Number}
	 */
	toRGB() { return (this.r << 16) + (this.g << 8) + this.b; }

	/**
	 * Clamps the rgb values of the Color object to the range of 0...255.
	 * The Color object itself is modified.
	 * @returns {Color} - this
	 */
	clamp() {

		this.r = ( this.r < 0 ) ? 0 : ( this.r > 255 ? 255 : this.r );
		this.g = ( this.g < 0 ) ? 0 : ( this.g > 255 ? 255 : this.g );
		this.b = ( this.b < 0 ) ? 0 : ( this.b > 255 ? 255 : this.b );

		return this;

	}

	/**
	 * Normalize the rgb components of this color.
	 * Useful for treating colors geometrically.
	 * @returns {Color} returns 'this' for chaining.
	 */
	normalizeRGB() {

		let len = this.r*this.r + this.g*this.g + this.b*this.b;
		if ( len === 0 ) {
			this.r = 1;
			return this;
		}
		len = Math.sqrt(len);

		this.r /= len;
		this.g /= len;
		this.b /= len;

		return this;
	}

	/**
	 * Returns a new Color that is the sum of this color and the given color.
	 * @param {Color} color
	 * @returns {Color}
	 */
	add( color ) {
		return new Color( this.r + color.r, this.g + color.g, this.b + color.b );
	}

	dist( color ) {
		let dr = this.r - color.r;
		let dg = this.g - color.g;
		let db = this.b - color.b;

		return Math.sqrt( dr*dr + dg*dg + db*db );
	}

	/**
	 * Returns a new Color that is the difference of this color and the given color.
	 * @param {Color} color
	 * @returns {Color}
	 */
	subtract( color ) {
		return new Color( this.r - color.r, this.g - color.g, this.b - color.b );
	}

	absDiffInt( n ) {
		return Math.abs( this.r - (0xFF&(n>>16)) ) + Math.abs( this.g - (0xFF&(n>>8)) ) + Math.abs( this.b - (0xFF&n) );
	}

	absDiffRGB( r, g, b ) {
		return Math.abs( this.r - r ) + Math.abs( this.g - g ) + Math.abs( this.b - b );
	}

	/**
	 * Computes the non-vector RgB cross-product with another color.
	 * Used to partition colors geometrically.
	 * @param {Color} color 
	 */
	crossRGB( color ) {
		return this.r*( color.g - color.b ) + this.g*( color.b - color.r) + this.b*( color.r - color.g );
	}

	/**
	 * Subtract the given origin from this Color and take the dot product with color.
	 * @param {Color} origin 
	 * @param {*} color 
	 */
	subDot( origin, color ) {
		return (this.r - origin.r)*color.r + (this.g - origin.g)*color.g + (this.b - origin.b)*color.b;
	}

	/**
	 * Get the cross product of this color, measured from the given origin,
	 * to the second color.
	 * Optimization function to avoid creating new Color objects.
	 * @param {Color} origin 
	 * @param {Color} color 
	 */
	subCross( origin, color ) {

		return (this.r - origin.r)*( color.g - color.b )
				+ (this.g - origin.g)*( color.b - color.r)
					+ (this.b - origin.b)*( color.r - color.g );
	}

	/**
	 * Returns the sum of the component-wise difference between this color and another color.
	 * @param {Color} color
	 * @returns {Number}
	 */
	diff( color ){

		return (this.a - color.a) + ( this.g - color.g ) + ( this.b - color.b );
	}

	/**
	 * Returns the sum of the absolute values of the component-wise difference between
	 * this color and another color.
	 * @param {Color} color
	 * @returns {Number}
	 */
	absDiff( color ) {

		return Math.abs(this.a - color.a) + Math.abs( this.g - color.g )
			+ Math.abs( this.b - color.b );

	}

}