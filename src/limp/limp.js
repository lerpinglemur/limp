
export default class Limp {

	get image() { return this._image; }
	set image(v) { this._image = v; }

	get loader() { return this._loader; }

	constructor() {
	}

	load( file ) {

		// cancel any load currently in progress.
		this.cancel();

		var reader = new FileReader();
		reader.onload = (evt)=>{

			var img = new Image();
			img.onloader = ()=>{
			};
			img.src = evt.target.result;
	
		};

		this._loader = reader;

		reader.readAsDataURL( file );

		return this.reader;

	}

	imgLoaded( evt ) {

		this._loader = null;
		var img = new Image();
			img.onloaded = ()=>{
		};
		img.src = evt.target.result;

	}

	/**
	 * Cancels an image load in progress, if any.
	 */
	cancel() {
		if ( this._loader ) this._loader.abort();
	}
}