import { EventEmitter } from "events";

export default class ImageReader extends EventEmitter {

	/**
	 * {FileReader}
	 */
	get reader() { return this._reader; }

	/**
	 * {Image} last image loaded.
	 */
	get image() { return this._image; }

	constructor(file=null) {

		super();
		if ( file ) this.load(file);

	}

	cancel() {

		if ( this._reader ) {
			this._reader.abort();
			this._reader = null;
		}

	}

	load( file ) {

		console.log('Loading image');
		var reader = new FileReader();
		reader.onload = (evt)=>this.loaded(evt);

		this._reader = reader;
		reader.readAsDataURL( file );

	}

	loaded( evt ) {

		console.log('image data read');
		this.emit( 'dataLoaded', evt.target.result );

		var img = new Image();
		img.onload = ()=>{
			console.log('image read.');
			this._image = img;
			this.emit( 'imageLoaded', img );
		};
		img.src = evt.target.result;

		if ( evt.target === this._reader ) this._reader = null;
	}

};