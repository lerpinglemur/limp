import Vue from 'vue';
import Main from 'view/main.vue';

/**
 * Globals applied to all vue components.
 */
Vue.mixin({

	methods: {
		// get a unique id for an html element, using the vue component uid.
		elmId(name) { return name + this._uid; }
	}

});

/**
 * Create new Vue instance, replacing vueRoot with the Main component.
 */
new Vue({

	el:'#vueRoot',
	components:{ Main },
	render( createElm ) {
		return createElm(Main);
	}

});